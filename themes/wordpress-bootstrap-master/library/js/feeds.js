(function($, _) {
    $.fn.strimFeeds = function() {
        var view = "<% _.each(json, function(list) { %>\n\
                    <div class='col-xs-4 feeds'>\n\
                            <% _.each(list, function(item) { %>\n\
                            <article>\n\
                                <header>\n\
                                    <h4>\n\
                                         <a href='<%= item.link %>'>\n\
                                         <%= item.title %>\n\
                                         </a>   \n\
                                    </h4>\n\
                                </header>\n\
                                <p>\n\
                                 <%= item.description %>\n\
                                </p>\n\
                            </article>\n\
                           <% }); %>\n\
                    </div>\n\
                    <% }); %>";

        return this.each(function() {
            var $o = $(this);
            var address = '/wp-content/themes/wordpress-bootstrap-master/library/js/bbc.php';
            function refresh() {
                $.ajax({
                    type: "GET",
                    url: address,
                    dataType: 'json',
                    error: function() {
                        console.log('Unable to load feed, Incorrect path or invalid feed');
                    },
                    success: function(json) {

                     $o.html(
                             _.template(view, {json: json})
                             );
                    }
                });
            }
            refresh();
            setInterval(function() {
                refresh();
            }, 100000);

        });
    }
})(jQuery, _);
jQuery(document).ready(function() {
    jQuery('.feeds').strimFeeds();
});
