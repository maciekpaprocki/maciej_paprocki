<?php
/*
  Template Name: Feed2
 */

function add_scripts_method() {
    wp_enqueue_script('feed', get_stylesheet_directory_uri() . '/library/js/feeds.js', array('underscore'));
}

add_action('wp_enqueue_scripts', 'add_scripts_method');
?>

<?php get_header(); ?>

<div id="content" class="clearfix row">

    <div id="main" class="col col-lg-12 clearfix" role="main">


        <div class="row feeds">

            <div class="col-xs-4 feeds" data-address="http://feeds.bbci.co.uk/news/rss.xml"> </div>


        </div>
    </div> <!-- end #main -->

<?php //get_sidebar(); // sidebar 1   ?>

</div> <!-- end #content -->

<?php get_footer(); ?>