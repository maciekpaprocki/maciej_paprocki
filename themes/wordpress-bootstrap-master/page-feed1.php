<?php
/*
  Template Name: Feed1
 */
?>

<?php get_header(); ?>

<div id="content" class="clearfix row">

    <div id="main" class="col col-lg-12 clearfix" role="main">
        <?php
        $feed = 'http://feeds.bbci.co.uk/news/rss.xml';
        $feeds = (array) simplexml_load_file($feed);
        $feed_array[] = array($feeds["channel"]->item[0],$feeds["channel"]->item[1],$feeds["channel"]->item[2]);
        $feed = 'http://feeds.bbci.co.uk/news/business/rss.xml';
        $feeds = (array) simplexml_load_file($feed);
        $feed_array[] = array($feeds["channel"]->item[0],$feeds["channel"]->item[1],$feeds["channel"]->item[2]);
        $feed = 'http://feeds.bbci.co.uk/news/technology/rss.xml';
        $feeds = (array) simplexml_load_file($feed);
        $feed_array[] = array($feeds["channel"]->item[0],$feeds["channel"]->item[1],$feeds["channel"]->item[2]);
        ?>
     
        <div class="row">
        <?php foreach ($feed_array as $list):?>
  
           
            <div class="col-xs-4">  
                <?php foreach ($list as $item):?>
                <article>
                    <header>
                        <h4>
                            <a href="<?php echo $item->link;?>">
                            <?php echo $item->title;?>
                            </a>
                        </h4>
                    </header>
                    <p>
                        <?php echo $item->description;?>
                    </p>
                </article>
                <?php endforeach;?>
            </div>
           
         <?php endforeach;?>
        </div>
    </div> <!-- end #main -->

    <?php //get_sidebar(); // sidebar 1  ?>

</div> <!-- end #content -->

<?php get_footer(); ?>